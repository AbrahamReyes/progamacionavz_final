#include <stdio.h>
#include <stdlib.h>
#include "component.h"
#include "node.h"
#include "functions.h"
#define UPWARD 0
#define DOWNWARD 1
#define resistence 11
#define	capacitance 12
#define inductance 13
#define vs 14
#define cs 15

int main(){
	CList mylist = NULL;
	nList nlist = NULL;
	int idnode = 1;
	int idnode2 = 2;
	char scale[2]="k";
	int cnode[]={1,2};
	char scale2[] = "k";
	double magnitude2 = 3.6;
	int c2node[] = {2,1};
	insertComponent(&mylist, 1,scale,4.3,11,cnode);
	insertComponent(&mylist, 2, scale2, magnitude2,11,c2node);
	showList(mylist, 0);
	insertNode(&nlist, idnode);
	insertNode(&nlist, idnode2);
	if(findNode(&nlist, 2))printf("Se encontró nodo \n");
	else printf("No se encontró nodo\n");
	int n = GetNumberOfNodes(nlist);
	printf("Numero de nodos: %d \n", n);
	int *matrix;
	matrix = (int*)malloc(n*n*sizeof(int));	
	creatematrix(n, mylist, matrix);
	/*print matrix*/
	for(int i=0;i<n;i++){
		for(int j=0;j<n;j++){
			printf(" %d ", matrix[i*n+j]);
		}
		printf("\n");
	}
	delete_list(&mylist);
	deleteList(&nlist);
return 0;
} 
