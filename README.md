# ProgamacionAVZ_Final
Integrantes: Sergio Rodriguez Bailón Reyes Almanza Jesús Abraham

Instalacion:


git clone https://gitlab.com/AbrahamReyes/progamacionavz_final.git





#Instalar Flex y Bison

Ubuntu

sudo apt-get update
sudo apt-get upgrade
sudo apt-get install flex bison

#Fedora 29

sudo yum install bison-devel
sudo yum install -y flex

#Instalar Cairo (Proyecto Visual)
Fedora 29
sudo yum install cairo-devel
sudo apt-get install libcairo2-dev

#Proyecto En Consola

##Compilacion
sh compilacion.sh

##Ejecución
./Xcircuit <archivo con el circuito>
