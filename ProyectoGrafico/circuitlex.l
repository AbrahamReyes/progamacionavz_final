%{
#include<string.h>
#include<stdlib.h>
#include "regex.tab.h"
void yyerror(char*);
int yyparse(void);
int yylineno;
%}

%option yylineno


EOL   \n
WS    [ \t]
EOT   ({WS}|{EOL})
DIGIT		[0-9]
scale		([YZEPTGMkhdcmunpfazy]|da)?
sign		"-"
node		N
res		R
ind		L
vs		"VS" 
cs		"CS"
cap		C
gr		"GR"
%%
#.*$ {
   /* any thing after '#' is a comment */
}
{WS} 
 {
	/* salta espacios en blanco */
 }

{EOL}
 {
	lines++;
	return '\n';
 }

{DIGIT}+		{ yylval.iv = atoi(yytext);return T_INT;}
{DIGIT}+"."{DIGIT}*	{ yylval.dv = atof(yytext);return T_FLOAT;}
{sign}			{ yylval.sv = strdup(yytext); return T_MIN;}
{gr}			{ yylval.sv = strdup(yytext); return T_G;}
{scale}			{ yylval.sv = strdup(yytext); return T_SCALE;}
{node}			{ yylval.sv = strdup(yytext); return T_NODE;}
{res}			{ yylval.sv = strdup(yytext); return T_RES;}
{ind}			{ yylval.sv = strdup(yytext); return T_IND;}
{vs}			{ yylval.sv = strdup(yytext); return T_VS;}
{cs}			{ yylval.sv = strdup(yytext); return T_CS;}
{cap}			{ yylval.sv = strdup(yytext);return T_CAP;}
%%

void yyerror(char* str)
{ 
	printf("\n Error: %s en linea %d", str, yylineno);
}
int yywrap(void) { }







