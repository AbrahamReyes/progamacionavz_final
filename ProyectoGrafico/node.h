#ifndef STDLIB
#define STDLIB
#include <stdlib.h>
#endif
typedef struct node{
	struct node *next_node;
	int id_node;
	struct node *previous_node;
}typeNode;

typedef typeNode *pNode;
typedef typeNode *nList;


void insertNode(nList *nodelist, int newId){
	pNode newNode, actual;
	newNode = (pNode)malloc(sizeof(typeNode));
	newNode->id_node = newId; 
	
	actual = *nodelist;
	if(actual)while(actual->previous_node)actual = actual->previous_node;
	if(!actual || actual->id_node > newId){
		newNode->next_node  = actual;
		newNode->previous_node = NULL;
		if(actual)actual->previous_node = newNode;
		if(!*nodelist) *nodelist = newNode; 
	}
	else {
		while(actual->next_node && actual->next_node->id_node <= newId )
			actual = actual->next_node;
			newNode->next_node = actual->next_node;
			actual->next_node = newNode;
			newNode->previous_node = actual;
			if(newNode->next_node)newNode->next_node->previous_node = newNode; 
	}
	printf("\n Se inserto nodo: %d", newId);	
}
void deleteList(nList *mynlist){
	pNode xNode, actual;

	actual = *mynlist;
	while(actual->previous_node)actual=actual->previous_node;

	while(actual){
		xNode = actual;
		actual = actual->next_node;
		free(xNode);
		
	}
	*mynlist = NULL;
}
// 0 si no encuentra nodo
// 1 si encuentra nodo
int findNode(nList *nodelist, int xid){
	pNode xnode;
	
	xnode = *nodelist;

//	printf("node id: %d\n", xnode->id_node);	
	//Go to the first node in the list
	while(xnode && xnode->id_node < xid)xnode=xnode->next_node;
	while(xnode && xnode->id_node > xid)xnode=xnode->previous_node;


	if(!xnode || xnode->id_node!=xid)return 0;
	return 1;	
	
}
int GetNumberOfNodes(nList nodelist){

	int numberN = 1;
	pNode xnode;
	xnode = nodelist;
	if (!nodelist) { return 0;}
	//Ir al final de la lista;
	while(xnode->next_node){xnode = xnode->next_node; numberN++;}
	//numberN = xnode->id_node;
	//ir al incio de la lista
	//while(xnode->previous_node)xnode = xnode->previous_node;
	//si el nodo empieza en 0 regresar numberN + 1
	//if(!xnode->id_node)return numberN+1;
	
	//else if(xnode->id_node==1)return numberN;
	return numberN+1;
}

