#ifndef UPWARD
#define UPWARD 0
#define DOWNWARD 1
#endif
//Structure for electronic components
typedef struct component{

	struct component *next;
	int id;
	char scale[2];
	double magnitude;
	int type_component;
	int nodes[2];
	struct component *previous;
} roleComponent;

typedef roleComponent *pComponent;
typedef roleComponent *CList;

void insertComponent(CList *mylist, int newId, char *newScale, double newMagnitude, int newType, int *newNodes){
	pComponent newC, actual;
	
	//Creating new component
	newC = (pComponent)malloc(sizeof(roleComponent));
	newC->id = newId;
	for(int i=0; i < 2;i++)newC->scale[i] = newScale[i];
	//strcpy(newC->scale, newScale);
	newC->magnitude = newMagnitude;
	newC->type_component = newType;
	for(int j=0;j<2;j++)newC->nodes[j] = newNodes[j];

	//setting the actual component to the list
	actual = *mylist;

	//set actual to the first element of the list
	if(actual)while(actual->previous)actual=actual->previous;
	
	//Empty CList of components
	if(!actual){
		newC->next = actual;
		newC->previous = NULL;
		if(!*mylist) *mylist = newC;
	}
	else{
		/*Going forward until the last element is reached*/
		while(actual->next)actual = actual->next;
			newC->next = actual->next;
			actual->next = newC;
			newC->previous = actual;
		
	
	}
	printf("\n Se inserto componente");
}
void delete_list(CList *mylist){
	pComponent xComponent, actual;

	actual = *mylist;

	while(actual){
		xComponent = actual;
		actual = actual->next;
		free(xComponent);
	}	
	*mylist=NULL;
}	
	
void showList(CList mylist, int order){
	pComponent xcomponent;
	xcomponent = mylist;
	if(!mylist){ printf("Error there is no electric components\n"); exit(1);}
	
	if(order==UPWARD){
		while(xcomponent->previous)xcomponent = xcomponent->previous;
		while(xcomponent){
			printf("id : %d \n", xcomponent->id);
			printf("scale : %s \n", xcomponent->scale);
			printf("Magnitude: %f \n", xcomponent->magnitude);
			printf("Type of component: %d\n", xcomponent->type_component);
			for(int i=0;i<2;i++)printf("Conect to node: %d\n",xcomponent->nodes[i]);
			xcomponent = xcomponent->next;
		}
		
	}
	else if(order==DOWNWARD){
		while(xcomponent->next)xcomponent = xcomponent->next;
		while(xcomponent){
			printf("id : %d \n", xcomponent->id);
			printf("scale : %s \n", xcomponent->scale);
			printf("Magnitude: %f \n", xcomponent->magnitude);
			printf("Type of component: %d\n", xcomponent->type_component);
			for(int i=0;i<2;i++)printf("Conect to node: %d\n",xcomponent->nodes[i]);
			xcomponent=xcomponent->previous;
		}
		
	}
	printf("\n");	
}

// devuelve 0 si lista vacia
// devuelve 1 de lo contrario
int isEmpty(CList mylist)
{
	pComponent xcomponent;
	xcomponent = mylist;
	if(!mylist) return 1;
	return 0;
}

void insertLastNode(CList nodelist, int node){

	pComponent xnode;
	xnode = nodelist;
	//Ir al final de la lista;
	while(xnode->next){xnode = xnode->next;}
	xnode->nodes[1] = node;
}
int getMaxNode(CList mylist){
	pComponent xcomponent;
	int currentmax=0, max=0;
	xcomponent = mylist;
	while(xcomponent->previous)xcomponent=xcomponent->previous;
	while(xcomponent){
			currentmax = (xcomponent->nodes[0]>xcomponent->nodes[1])?xcomponent->nodes[0]:xcomponent->nodes[1];
			max = (max>currentmax)?max:currentmax;
			xcomponent = xcomponent->next;
	}
return max;

}
int findComponentById(CList mylist, int idx){
        pComponent xcomponent;
        xcomponent=mylist;
        while(xcomponent->previous)xcomponent = xcomponent->previous;
        while(xcomponent){
				
		//printf("\nID buscado: %d\n", idx);
                if((xcomponent->id)==idx){return 1;}
                xcomponent= xcomponent->next;

        }
        return 0;
}	
// Revisa que un componente no este repetido
// Regresa 1 si encontro ese componente repetido
// Regresa 0 si no encontro ese componente repetido
int existComponent(CList mylist, int id, int type)
{
        pComponent xcomponent;
        xcomponent=mylist;
        while(xcomponent)
	{
		if(xcomponent->id==id && xcomponent->type_component == type) return 1;
		xcomponent= xcomponent->next;

        }
        return 0;
}

