%{
#include <stdio.h>
#include <stdlib.h>
#include "circuit.h"
#include <string.h>


extern int yylex();
extern void yyerror(char*);

nList nlist = NULL;
CList mylist = NULL;
int cnode[]={1,1};



char scale[] = {'\0', '\0'};


%}
%locations


%union {
   int iv;
   double dv;
   char *sv;
};

%token <sv> T_NODE T_RES T_IND T_VS T_CS T_CAP T_SCALE T_G T_MIN
%token <iv> T_INT
%token <dv> T_FLOAT

%start parsetree
%%

parsetree:
circuito
;

circuito:
circuito linea
|linea
;

linea:
nodo componente
;


nodo:
T_NODE T_INT 
{
	printf("\n La lista sera vacia?");
	if(!isEmpty(mylist))
	{
		insertLastNode(mylist, $2);printf("\n Se inserto nodo para ultimo comp"); 
	}
	printf("\n Pasamos la lista de componentes");
	printf("\n\t Nodo: %s%d", $1, $2); 
	if(!GetNumberOfNodes(&nlist) || !findNode(&nlist, $2))
		insertNode(&nlist, $2);
	printf("\n Se inserto un nodo");
	cnode[0] = $2;
	//if(id > 0) insertComponent(&mylist, id, scale, magnitude, type, cnode);
}
;

componente:
|resistencia
|inductancia
|fuente	
|capacitor
|tierra
;
resistencia: 
T_RES T_INT T_FLOAT			{ 
						//printf("\n\t Resistencia11: %s%d %9.2f", $1, $2, $3);
						if(existComponent(mylist, $2, 11))
						{
							printf("\nError de sintaxis, componente repetido\n");
							exit(1);
						} 
						insertComponent(&mylist, $2, scale, $3, 11, cnode);
}

|T_RES T_INT T_INT			{
						//printf("\n\t Resistencia: %s%d %d", $1, $2, $3);
						if(existComponent(mylist,$2, 11))
						{
							printf("\nError de sintaxis, componente repetido\n"); 
							exit(1);
						}
						insertComponent(&mylist, $2, scale, $3, 11, cnode);}
|T_RES T_INT T_FLOAT T_SCALE		{
						//printf("\n\t Resistencia: %s%d %9.2f %s", $1, $2, $3, $4);
						if(existComponent(mylist,$2, 11))
						{
							printf("\nError de sintaxis, componente repetido\n"); 
							exit(1);
						}
						insertComponent(&mylist, $2, $4, $3, 11, cnode);
					}
|T_RES T_INT T_INT T_SCALE		{
						//printf("\n\t Resistencia: %s%d %d %s", $1, $2, $3, $4);
						if(existComponent(mylist,$2, 11))
						{
							printf("\nError de sintaxis, componente repetido\n"); 
							exit(1);
						}
						insertComponent(&mylist, $2, $4, $3, 11, cnode);
					}
;

inductancia: 
T_IND T_INT T_FLOAT			{
						//printf("\n\t Inductancia13: %s%d %9.2f", $1, $2, $3);
						if(existComponent(mylist,$2, 13))
						{
							printf("\nError de sintaxis, componente repetido\n"); 
							exit(1);
						}
						insertComponent(&mylist, $2, scale, $3, 13, cnode);
					}

|T_IND T_INT T_FLOAT T_SCALE		{
						//printf("\n\t Inductancia: %s%d %9.2f %s", $1, $2, $3, $4);
						if(existComponent(mylist,$2, 13))
						{
							printf("\nError de sintaxis, componente repetido\n"); 
							exit(1);
						}
						insertComponent(&mylist, $2, $4, $3, 13, cnode);
					}
|T_IND T_INT T_INT			{
						//printf("\n\t Inductancia: %s%d %d", $1, $2, $3);
						if(existComponent(mylist,$2, 13))
						{
							printf("\nError de sintaxis, componente repetido\n"); 
							exit(1);
						}
						insertComponent(&mylist, $2, scale, $3, 13, cnode);
					}
|T_IND T_INT T_INT T_SCALE		{
						//printf("\n\t Inductancia: %s%d %d %s", $1, $2, $3, $4);
						if(existComponent(mylist,$2, 13))
						{
							printf("\nError de sintaxis, componente repetido\n"); 
							exit(1);
						}
						insertComponent(&mylist, $2, $4, $3, 13, cnode);
					}
;

fuente:
T_VS T_INT T_FLOAT			{
						//printf("\n\t Fuente14: %s%d %9.2f", $1, $2, $3);
						if(existComponent(mylist,$2, 14))
						{
							printf("\nError de sintaxis, componente repetido\n"); 
							exit(1);
						}
						insertComponent(&mylist, $2, scale, $3, 14, cnode);
					}
| T_VS T_INT T_MIN T_FLOAT		{
						//printf("\n\t Fuente: %s%d %s%9.2f", $1, $2, $3, $4);
						if(existComponent(mylist,$2, 14))
						{
							printf("\nError de sintaxis, componente repetido\n"); 
							exit(1);
						}
						insertComponent(&mylist, $2, scale, -($4), 14, cnode);
					}
|T_VS T_INT T_FLOAT T_SCALE		{
						//printf("\n\t Fuente: %s%d %9.2f %s", $1, $2, $3, $4);
						if(existComponent(mylist,$2, 14))
						{
							printf("\nError de sintaxis, componente repetido\n"); 
							exit(1);
						}
						insertComponent(&mylist, $2, $4, $3, 14, cnode);
					}
| T_VS T_INT T_MIN T_FLOAT T_SCALE	{
						//printf("\n\t Fuente: %s%d %s%9.2f %s", $1, $2, $3, $4, $5);
						if(existComponent(mylist,$2, 14))
						{
							printf("\nError de sintaxis, componente repetido\n"); 
							exit(1);
						}
						insertComponent(&mylist, $2, $5, -($4), 14, cnode);
					}
;

capacitor:
T_CAP T_INT T_FLOAT			{
						//printf("\n\t Capacitor12: %s%d %9.2f", $1, $2, $3);
						if(existComponent(mylist,$2, 12))
						{
							printf("\nError de sintaxis, componente repetido\n"); 
							exit(1);
						}
						insertComponent(&mylist, $2, scale, $3, 12, cnode);
					}
|T_CAP T_INT T_FLOAT T_SCALE		{
						//printf("\n\t Capacitor: %s%d %9.2f %s", $1, $2, $3, $4);
						if(existComponent(mylist,$2, 12))
						{
							printf("\nError de sintaxis, componente repetido\n"); 
							exit(1);
						}
						insertComponent(&mylist, $2, $4, $3, 12, cnode);
					}
|T_CAP T_INT T_INT			{
						//printf("\n\t Capacitor: %s%d %d", $1, $2, $3);
						if(existComponent(mylist,$2, 12))
						{
							printf("\nError de sintaxis, componente repetido\n"); 
							exit(1);
						}
						insertComponent(&mylist, $2, scale, $3, 12, cnode);
					}
|T_CAP T_INT T_INT T_SCALE		{
						//printf("\n\t Capacitor: %s%d %d %s", $1, $2, $3, $4);
						if(existComponent(mylist,$2, 12))
						{
							printf("\nError de sintaxis, componente repetido\n"); 
							exit(1);
						}
						insertComponent(&mylist, $2, $4, $3, 12, cnode);
					}
;

tierra:
T_G T_INT				{
						//printf("\n\t Tierra: %s", $1);
						if(existComponent(mylist,$2, 16))
						{
							printf("\nError de sintaxis, componente repetido\n"); 
							exit(1);
						}
						insertComponent(&mylist, $2, scale, 0, 16, cnode);
					}
;

%%
int main(int argc, char **argv)
{
    extern FILE *yyin;
    extern FILE *yyout;
    extern int yyparse(void);
	struct userData *mydata;
	mydata = malloc(sizeof(struct userData));
    if(argc  != 2)
    {
        printf("usage: ./ parser1  filename\n");
        exit (0);
    }
    FILE* file = fopen(argv[1],"r");
    if(file == NULL)
    {
        printf("couldn ’t open %s\n", argv [1]);  exit (0);
    }
    yyin = file;   // now  flex  reads  from  file
    yyparse ();
    fclose(file);
    printf("\n Numero de nodos: %d\n", getMaxNode(mylist));
    int n = getMaxNode(mylist);
    int *matrix;
    matrix = (int*)malloc(n*n*sizeof(int));
    memset(matrix,0,n*n*sizeof(int));
    creatematrix(n, mylist, matrix);
	printf("\nMatriz de adyancencia: \n");
    printMatrix(matrix,n);	
	printf("Cargando datos para desplegar graficamente...\n");
	mydata->mylistOfComponents = mylist;
	//mydata->Matrix = matrix;
	mydata->n = n;
	printf("Datos cargados. \n");

	GtkWidget *window;
	GtkWidget *darea;
	
	gtk_init(&argc, &argv);
	
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	
	darea = gtk_drawing_area_new();
	gtk_container_add(GTK_CONTAINER(window), darea);
	
	g_signal_connect(G_OBJECT(darea), "draw", G_CALLBACK(on_draw_event), mydata);
	g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL); 

	gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
	gtk_window_set_default_size(GTK_WINDOW(window), 1000,1000);
	gtk_window_set_title(GTK_WINDOW(window), "CCircuit");

	gtk_widget_show_all(window);
	
	gtk_main();

	//delete_list(&mylist);
	deleteList(&nlist);

    printf("\n\n");
    showList(mylist, 1);
    printf("\n");
	delete_list(&mylist);
}

