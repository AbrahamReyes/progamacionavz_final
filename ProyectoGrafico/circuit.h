#include <cairo.h>
#include <gtk/gtk.h>
#include <math.h>
#include "node.h"
#include "functions.h"
#define vertical 111
#define horizontal 222
#define up 333
#define down 444
#include <string.h>
#ifndef resistence
#define resistence 11
#define capacitance 12
#define inductance 13
#define vs 14
#define cs 15
#define ground 16
#endif

struct userData{
    CList mylistOfComponents;
    int *Matrix;
    int n;
};



static int drawCapacitor(cairo_t *cr,int x0, int y0, int position,int id);
static int drawResistence(cairo_t *cr,int x0, int y0, int position, int id);
static int drawInductance(cairo_t *cr,int x0, int y0, int position, int id);
static int drawSourceVoltage(cairo_t *cr,int x0, int y0, int position, int id);
static int drawSourceCurrent(cairo_t *cr,int x0, int y0, int position, int orientation, int id);
static void drawGround(cairo_t *cr,int x0, int y0, int position);
static int drawline(cairo_t *cr, int x0, int y0, int position);




void printMatrix(int *M,int n){
	for(int i=0;i<n;i++){
		for(int j=0;j<n;j++){
			printf("%d ", M[i*n+j]);
		}
		printf("\n");	
	}

}
static gboolean on_draw_event(GtkWidget *widget, cairo_t *cr,
	gpointer user_data){
	struct userData *myinfo = user_data;
	CList components = myinfo->mylistOfComponents;
	pComponent xcomponent;
	xcomponent = components;
	while(xcomponent->previous)xcomponent=xcomponent->previous;
	int n = getMaxNode(components);
	int position=horizontal;	
	int *matrix = myinfo->Matrix;
	int componentype = 0;
	int posx = 50, posy=50;
	int x, y;
	for(int i=0;i<n;i++){
		for(int j=0;j<n;j++){
			while(xcomponent){
			//xcomponent = findComponent(components, i+1, j+1); 
			int xid = xcomponent->id;
				    xid = xcomponent->id;
					componentype=xcomponent->type_component;	
					switch(componentype){

						case resistence:
							posx=drawResistence(cr, posx, posy, position,xid);
						break;
						case capacitance:
							posx=drawCapacitor(cr, posx,posy, position,xid);
						break;
						case inductance:
							posx=drawInductance(cr, posx, posy,position,xid);
						break;
						case vs:
							posx=drawSourceVoltage(cr, posx,posy,position,xid);
						break;
						case cs:
							posx=drawSourceCurrent(cr,posx, posy, position,up,xid);
						break;
						case ground:
							drawGround(cr, 100, 100, vertical);
						break;
					}
						//posx = posx+100;
				 xcomponent=xcomponent->next;	
		  }
		}
	}
	
	return FALSE;
}

static int drawCapacitor(cairo_t *cr,int x0,int y0, int position, int id){
	int sizeC = 20, pos=0;
	char cname[10] = "C";
	char idc[2];
	sprintf(idc, "%ld", id);
	strcat(cname,idc);
    cairo_set_source_rgb(cr, 1, 1, 0);
	cairo_select_font_face(cr, "Purisa",CAIRO_FONT_SLANT_NORMAL,CAIRO_FONT_WEIGHT_BOLD);
	cairo_set_font_size(cr, 13);
	
	if(position == vertical){
			cairo_move_to(cr, (x0+sizeC)/4, y0+(3*sizeC)/4);
			cairo_show_text(cr, cname);	
			cairo_move_to (cr, x0, y0);
			cairo_rel_line_to(cr, 0,-sizeC);
			cairo_move_to (cr, x0-sizeC/2, y0);
			cairo_rel_line_to(cr, sizeC, 0);
			cairo_move_to(cr, x0, y0+sizeC);
			cairo_move_to(cr, x0-sizeC/2, y0+sizeC);
			cairo_rel_line_to(cr, sizeC,0);
			cairo_move_to(cr, x0, y0+sizeC);
			cairo_rel_line_to(cr, 0, sizeC);
			cairo_set_line_width (cr, 0.9);
			pos = y0+sizeC*2;
	}
	else if(position == horizontal){
			cairo_move_to(cr, x0,(y0+sizeC)/2);
			cairo_show_text(cr, cname);	
			cairo_move_to (cr, x0, y0);
			cairo_rel_line_to(cr, -sizeC, 0);
			cairo_move_to (cr, x0, y0-sizeC/2);
			cairo_rel_line_to(cr, 0, sizeC);
			cairo_move_to(cr, x0+sizeC, y0);
			cairo_move_to(cr, x0+sizeC, y0-sizeC/2);
			cairo_rel_line_to(cr, 0, sizeC);
			cairo_move_to(cr, x0+sizeC, y0);
			cairo_rel_line_to(cr, sizeC, 0);
			cairo_set_line_width (cr, 0.9);
			pos = x0 + sizeC*2;
	}

	cairo_stroke(cr); 
	return pos;
}
static int drawResistence(cairo_t *cr,int x0, int y0, int position, int id){
	int sizeC = 20, pos=0;
	char cname[10] = "R";
	char idc[2];
	sprintf(idc, "%ld", id);
	strcat(cname,idc);

    cairo_set_source_rgb(cr, 1, 1, 0);
	cairo_select_font_face(cr, "Purisa",CAIRO_FONT_SLANT_NORMAL,CAIRO_FONT_WEIGHT_BOLD);
	cairo_set_font_size(cr, 13);

	if(position == horizontal){
			cairo_move_to(cr, (x0+sizeC+10), (y0+sizeC)/2); 
            cairo_show_text(cr, cname);
			cairo_move_to (cr, x0, y0);
			cairo_rel_line_to(cr,sizeC,0);
			cairo_rel_line_to(cr, 0, sizeC/2);
			cairo_rel_line_to(cr, sizeC, -sizeC);
			cairo_rel_line_to(cr, 0, sizeC);
			cairo_rel_line_to(cr, sizeC, -sizeC);
			cairo_rel_line_to(cr, 0, sizeC/2);
			cairo_rel_line_to(cr, sizeC, 0);
			cairo_move_to(cr, x0+4*sizeC, y0);
			cairo_set_line_width (cr, 0.9);
			pos = x0+4*sizeC;

	}
	if(position == vertical){
			cairo_move_to(cr, (x0+sizeC)/4, (y0+sizeC*2)); 
            cairo_show_text(cr, cname);
			cairo_move_to (cr, x0, y0);
			cairo_rel_line_to(cr,0, sizeC);
			cairo_rel_line_to(cr, sizeC/2, 0);
			cairo_rel_line_to(cr, -sizeC, sizeC);
			cairo_rel_line_to(cr, sizeC, 0);
			cairo_rel_line_to(cr, -sizeC, sizeC);
			cairo_rel_line_to(cr, sizeC/2, 0);
			cairo_rel_line_to(cr, 0, sizeC);
			cairo_move_to(cr, x0, 4*sizeC);
			cairo_set_line_width (cr, 0.9); 
			pos = y0+4*sizeC;
				}
	
	cairo_stroke(cr); 
	return pos;
}
static int drawInductance(cairo_t *cr,int x0, int y0, int position, int id){
	int sizeC = 20, pos=0;
	char cname[10] = "I";
    char idc[2];
    sprintf(idc, "%ld", id);
    strcat(cname,idc);	
    cairo_set_source_rgb(cr, 1, 1, 0);
	cairo_select_font_face(cr, "Purisa",CAIRO_FONT_SLANT_NORMAL,CAIRO_FONT_WEIGHT_BOLD);
	cairo_set_font_size(cr, 13);

	if(position == horizontal){
			cairo_move_to(cr, x0+sizeC+5, (y0+sizeC)/2); 
            cairo_show_text(cr, cname);
			cairo_move_to (cr, x0, y0);
			cairo_rel_line_to(cr,sizeC,0);
			for(int i=0;i<4;i++)
			cairo_rel_curve_to (cr, 5, -55, 15, 55, 5, 0);
			cairo_rel_line_to(cr,sizeC,0);
			cairo_move_to(cr,x0+4*5+2*sizeC,0);
			cairo_set_line_width (cr, 0.9);
			pos = x0+4*5+3*sizeC; 
	}
	if(position == vertical){
			cairo_move_to(cr,(x0+sizeC)/4, y0+2*sizeC); 
            cairo_show_text(cr, cname);
			cairo_move_to (cr, x0, y0);
			cairo_rel_line_to(cr,0,sizeC);
			for(int i=0;i<4;i++)
			cairo_rel_curve_to (cr, -55, 5, 55, 15, 0, 5);
			cairo_rel_line_to(cr,0,sizeC);
			cairo_move_to(cr,0,x0+4*5+2*sizeC);
			cairo_set_line_width (cr, 0.9);
			pos=y0+4*5+3*sizeC;
	}

	cairo_stroke(cr); 
	return pos;
}
static int drawSourceVoltage(cairo_t *cr,int x0, int y0, int position, int id){
	int sizeC = 20, pos=0;
	char cname[10] = "V";
    char idc[2];
    sprintf(idc, "%ld", id);
    strcat(cname,idc);
    cairo_set_source_rgb(cr, 1, 1, 0);
	cairo_select_font_face(cr, "Purisa",CAIRO_FONT_SLANT_NORMAL,CAIRO_FONT_WEIGHT_BOLD);
	cairo_set_font_size(cr, 13);

	if(position == vertical){
			cairo_move_to(cr, (y0+sizeC)/4, y0+sizeC/2); 
            cairo_show_text(cr, cname);
			cairo_move_to (cr, x0, y0);
			cairo_rel_line_to(cr, 0,-sizeC);
			cairo_move_to (cr, x0-sizeC/4, y0);
			cairo_rel_line_to(cr, sizeC/2, 0);
			cairo_move_to(cr, x0, y0+sizeC);
			cairo_move_to(cr, x0-sizeC/2, y0+sizeC/2);
			cairo_rel_line_to(cr, sizeC,0);
			cairo_move_to(cr, x0, y0+sizeC/2);
			cairo_rel_line_to(cr, 0, sizeC);
			cairo_set_line_width (cr, 0.9);
			pos = y0+sizeC/2+2*sizeC;
	}
	if(position == horizontal){
			cairo_move_to(cr, x0-5, (y0+sizeC)/2); 
            cairo_show_text(cr, cname);
			cairo_move_to (cr, x0, y0);
			cairo_rel_line_to(cr, -sizeC, 0);
			cairo_move_to (cr, x0, y0-sizeC/4);
			cairo_rel_line_to(cr, 0, sizeC/2);
			cairo_move_to(cr, x0+sizeC, y0);
			cairo_move_to(cr, x0+sizeC/2, y0-sizeC/2);
			cairo_rel_line_to(cr, 0, sizeC);
			cairo_move_to(cr, x0+sizeC/2, y0);
			cairo_rel_line_to(cr, sizeC, 0);
			cairo_set_line_width (cr, 0.9);
			pos = x0+sizeC/2+2*sizeC;
	}



	cairo_stroke(cr); 
	return pos;
}
static int drawSourceCurrent(cairo_t *cr,int x0, int y0, int position, int orientation, int id){
	int sizeC = 20, pos =0;
	char cname[10] = "CS";
    char idc[2];
    sprintf(idc, "%ld", id);
    strcat(cname,idc);
    cairo_set_source_rgb(cr, 1, 1, 0);
	double rad = 0;	
	cairo_select_font_face(cr, "Purisa",CAIRO_FONT_SLANT_NORMAL,CAIRO_FONT_WEIGHT_BOLD);
	cairo_set_font_size(cr, 13);

	switch(position){
		case vertical:
			if(orientation==up){
				rad=-M_PI/2;
			}
			else if(orientation==down){
				rad=M_PI/2;
			}
			break; 
		case horizontal:
			if(orientation==up){
				rad=2*M_PI;
			}
			else if(orientation==down){
				rad=-M_PI;
			}
		break;
	
	}		
    if(position==horizontal){	
		cairo_move_to(cr, x0-sizeC+5, (y0+sizeC-10)/2); 
    	cairo_show_text(cr, cname);
		cairo_move_to(cr, x0,y0);
	}
	else if(position==vertical){
		cairo_move_to(cr,  x0+sizeC, y0+(1*sizeC)/4); 
    	cairo_show_text(cr, cname);
		cairo_move_to(cr, x0,y0);

	} 
	
	cairo_translate(cr, x0, y0);
	cairo_rotate(cr,rad);
	cairo_translate(cr, x0, y0);
	cairo_arc(cr, -x0, -y0, 20, 0, 2 * M_PI);
	cairo_move_to(cr, -x0+sizeC, -y0);
	cairo_rel_line_to(cr, -10,-10);
	cairo_move_to(cr, -x0+sizeC, -y0);
	cairo_rel_line_to(cr, -2*sizeC,0);
	
	cairo_move_to(cr, -x0+sizeC, -y0);
	cairo_rel_line_to(cr, -10,10);
	
	cairo_move_to(cr, x0+sizeC, y0);
	pos = x0+sizeC;
	cairo_stroke(cr); 
	return pos;
}
static void drawGround(cairo_t *cr,int x0, int y0, int position){
	int sizeC = 20, pos=0;
    cairo_set_source_rgb(cr, 1, 1, 0);

	if(position == vertical){
			cairo_move_to (cr, x0, y0);
			cairo_rel_line_to(cr, 0,-sizeC);
			cairo_move_to (cr, x0-sizeC/2, y0);
			cairo_rel_line_to(cr, sizeC, 0);
			cairo_move_to(cr, x0-8, y0+sizeC/4);
            cairo_rel_line_to(cr,(3*sizeC)/4,0);			
			cairo_move_to(cr, x0, y0+sizeC);
			cairo_move_to(cr, x0-sizeC/4, y0+sizeC/2);
			cairo_rel_line_to(cr, sizeC/2,0);
			cairo_move_to(cr, x0, y0+sizeC/2);
			cairo_rel_line_to(cr, 0, sizeC);
			cairo_set_line_width (cr, 0.9);
			pos = y0;
	}
	if(position == horizontal){
			cairo_move_to (cr, x0, y0);
			cairo_rel_line_to(cr, -sizeC, 0);
			cairo_move_to (cr, x0, y0-sizeC/4);
			cairo_rel_line_to(cr, 0, sizeC/2);
			cairo_move_to(cr, x0+sizeC/4, y0-8);
			cairo_rel_line_to(cr, 0,(3*sizeC)/4);
			cairo_move_to(cr, x0+sizeC, y0);
			cairo_move_to(cr, x0+sizeC/2, y0-sizeC/2);
			cairo_rel_line_to(cr, 0, sizeC);
			cairo_move_to(cr, x0+sizeC/2, y0);
			cairo_rel_line_to(cr, sizeC, 0);
			cairo_set_line_width (cr, 0.9);
			pos = x0;
	}



	cairo_stroke(cr); 
}

 
