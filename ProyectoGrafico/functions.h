#ifndef LIB
#define LIB
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "component.h"
#endif
void insertInMatrix(int *M,int n, int i, int j){
	for(int x=0;x<n;x++){
		for(int y=0;y<n;y++){
			if ((x==j-1 && y==i-1)||(x==j-1 && y==i-1))
				M[x*n+y]=1;
		}
	}

} 
void printResistance(){
	printf("Resistance");
}
void creatematrix(int n, CList listofComponents, int *matrix){
   	pComponent xcomponent;		
	xcomponent = listofComponents;
	if(!listofComponents){ printf("Error there is no electric components\n"); exit(1); }

	while(xcomponent->previous)xcomponent = xcomponent->previous;
	while(xcomponent){
		//printf("The component with id%d is conected with node %d and node %d \n", xcomponent->id, xcomponent->nodes[0], xcomponent->nodes[1]);	
		insertInMatrix((int *)matrix, n, xcomponent->nodes[0], xcomponent->nodes[1]);
		xcomponent = xcomponent->next;
	}
}
